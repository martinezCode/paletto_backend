<?php namespace Modules\Productos\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('Productos'), function (Item $item) {
                $item->icon('fa fa-tag');
                $item->weight(1);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('Producto'), function (Item $item) {
                    $item->icon('');
                    $item->weight(0);
                    $item->append('admin.productos.producto.create');
                    $item->route('admin.productos.producto.index');
                    $item->authorize(
                        $this->auth->hasAccess('productos.productos.index')
                    );
                });
// append

            });
        });

        return $menu;
    }
}
