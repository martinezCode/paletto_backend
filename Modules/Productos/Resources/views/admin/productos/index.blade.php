@extends('layouts.master')

@section('content-header')
    {{-- <h1>
        Productos
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ "Productos" }}</li>
    </ol> --}}
@stop

@section('content')
    <br>
    <h1>
        Productos
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ "Productos" }}</li>
    </ol>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.productos.producto.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ "Crear Nuevo Producto" }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">

                    <table class="data-table table table-bordered table-hover" id="tablaProductos">
                        <thead>
                            <tr>
                                <th>Codigo Producto</th>
                                <th>Descripcion</th>
                                <th>Activo</th>
                                <th>Archivo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>  
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Codigo Producto</th>
                            <th>Descripcion</th>
                            <th>Activo</th>
                            <th>Archivo</th>
                            <th>Acciones</th>
                        </tr>
                        </tfoot>
                    </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('productos::productos.title.create producto') }}</dd>
    </dl>
@stop

@section('scripts')
   {!! Theme::script('js/ekko-lightbox.min.js') !!}

    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $( document ).ready(function() 
        {
            var table = $('.data-table').DataTable(
            {
                dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                "<'row'<'col-xs-12't>>"+
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                "deferRender": true,
                processing: false,
                serverSide: true,
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ;?>'
                },
                ajax: 
                 {
                    url: '{!! route('admin.productos.producto.index_ajax') !!}',
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: function (e) 
                    {
                        e.categoria = $('#categoria').val();
                    }
                },
                columns: 
                [
                    { data: 'cod_producto', name: 'cod_producto' },
                    { data: 'descripcion' , name: 'descripcion' },
                    { data: 'activo' , name: 'activo' },
                    { data: 'archivo' , name: 'archivo', orderable: false, searchable: false },
                    { data: 'action', name: 'action', orderable: false, searchable: false} 
                ]
            });
        });
    </script>
@stop
