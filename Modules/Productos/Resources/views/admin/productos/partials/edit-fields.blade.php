<div class="box-body">
    	{!! Form::normalInput('DESCRIPCION', 'Nombre', $errors ,$producto) !!}
        {!! Form::normalInput('COD_PRODUCTO', 'Codigo', $errors,$producto) !!}
		{!! Form::normalCheckbox('ACTIVO', 'Activo', $errors,$producto) !!}
       

        @include('media::admin.fields.file-link', 
        [
            'entityClass' => 'Modules\\\\Productos\\\\Entities\\\\Producto',
            'entityId' => $producto->id,
            'zone' => 'archivo'
        ])
</div>
