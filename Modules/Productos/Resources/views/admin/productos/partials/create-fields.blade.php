<div class="box-body">
    <p>
        {!! Form::normalInput('DESCRIPCION', 'Nombre', $errors) !!}
        {!! Form::normalInput('COD_PRODUCTO', 'Codigo', $errors) !!}
		{!! Form::normalCheckbox('ACTIVO', 'Activo', $errors) !!}

        @include('media::admin.fields.new-file-link-single', 
    	[
    		'zone' => 'archivo'
		])
    </p>
</div>
