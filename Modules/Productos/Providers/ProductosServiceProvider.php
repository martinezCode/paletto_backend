<?php namespace Modules\Productos\Providers;

use Illuminate\Support\ServiceProvider;

class ProductosServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Productos\Repositories\ProductoRepository',
            function () {
                $repository = new \Modules\Productos\Repositories\Eloquent\EloquentProductoRepository(new \Modules\Productos\Entities\Producto());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Productos\Repositories\Cache\CacheProductoDecorator($repository);
            }
        );
// add bindings

    }
}
