<?php namespace Modules\Productos\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Productos\Entities\Producto;
use Modules\Productos\Repositories\ProductoRepository;
use Modules\Productos\Events\ProductoWasCreated;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Yajra\Datatables\Facades\Datatables;
use Modules\Media\Repositories\FileRepository;

class ProductoController extends AdminBaseController
{
    /**
     * @var ProductoRepository
     */
    private $producto;

    public function __construct(ProductoRepository $producto)
    {
        parent::__construct();

        $this->producto = $producto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$productos = $this->producto->all();

        return view('productos::admin.productos.index', compact(''));
    }

    public function index_ajax(Request $request)
    {
        // dd($request->all());

<<<<<<< HEAD
        $query = Producto::orderBy('Productos.DESCRIPCION', 'asc')
=======
        $query =Producto::orderBy('Productos.DESCRIPCION', 'asc')
>>>>>>> 723e2cc45c50244d3d09a9e4a20c8153b15e792d
                        ->select([
                                    'Productos.id as id',
                                    'Productos.COD_PRODUCTO as cod_producto', 
                                    'Productos.DESCRIPCION as descripcion', 
                                    'Productos.ACTIVO as activo', 
                                    // 'Productos.archivo as archivo', 
                                ]);

        $object = Datatables::of( $query )
            ->addColumn('action', function ($tabla) 
            {
                $asEdit = "admin.productos.producto.edit";

                $asDestroy = "admin.productos.producto.destroy";

                $editRoute = route( $asEdit, [$tabla->id]);

                $deleteRoute = route( $asDestroy, [$tabla->id]);

                $buttons="  <a href='". $editRoute." ' class='btn btn-default btn-flat'>
                                <i class='fa fa-pencil'></i>
                            </a>
                            
                            <button class='btn btn-danger btn-flat' data-toggle='modal' data-target='#modal-delete-confirmation' data-action-target='". $deleteRoute ."'>
                                <i class='fa fa-trash'></i>
                            </button>
                            
                        </div>";

                return $buttons;
            })
            
            ->filter(function ($query) use ($request)
            {
                if ($request->has('nombre')  && trim($request->has('nombre') !== '') )
                {
                    $query->where('Productos.nombre', 'like', "%{$request->get('nombre')}%");
                }

                if ($request->has('activo')  && trim($request->has('activo') !== '') )
                {
                    $query->where('activo',$request->get('activo') );
                }
            })
            
            ->editColumn('activo',' @if($activo)
                                        SI
                                    @else
                                        NO
                                    @endif')

            ->editColumn('archivo', '@if($archivo)<a href="{{$archivo }}" data-toggle="lightbox" onclick="event.preventDefault();$(this).ekkoLightbox();">
                                        <img src="{{ $archivo }}" class="img-thumbnail" alt="" class="" style="width: 60px; height: 50px;">
                                    </a>@endif')
            ->make(true);

          
       
        return $object;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('productos::admin.productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $producto = $this->producto->create($request->all());

        event(new ProductoWasCreated($producto, $request->all()));

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('productos::productos.title.productos')]));

        return redirect()->route('admin.productos.producto.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Producto $producto
     * @return Response
     */
    public function edit(Producto $producto , FileRepository $FileRepository)
    {
        $archivo = $FileRepository->findFileByZoneForEntity('archivo',$producto);
        return view('productos::admin.productos.edit', compact('producto','archivo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Producto $producto
     * @param  Request $request
     * @return Response
     */
    public function update(Producto $producto, Request $request)
    {
        // dd($request->all());
        $this->producto->update($producto, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('productos::productos.title.productos')]));

        return redirect()->route('admin.productos.producto.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Producto $producto
     * @return Response
     */
    public function destroy(Producto $producto)
    {
        $this->producto->destroy($producto);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('productos::productos.title.productos')]));

        return redirect()->route('admin.productos.producto.index');
    }

}
