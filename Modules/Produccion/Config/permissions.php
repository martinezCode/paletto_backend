<?php

return [
    'produccion.produccions' => [
        'index',
        'index_ajax',
        'actualizar_produccion',
        'index_cliente',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
    'produccion.entregas' => [
        'index',
        'index_ajax',
        'index_reposicion',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
// append


];
