<?php namespace Modules\Produccion\Entities;

use Dimsav\Translatable\Translatable;
use Modules\Media\Support\Traits\MediaRelation;
use Illuminate\Database\Eloquent\Model;

class Produccion extends Model
{
    use  MediaRelation;
    
    protected $table = 'Sugerido';

    protected $fillable = 
    [
    	'COD_CONTACTO',
    	'COD_PRODUCTO',
    	'DESCRIPCION',
    	'CANTIDAD',
    	'idem',
    	'id_ser',
    	'Sector',
    	'user',
    	'FECHA',
    	'id_pro',
    	'syncm',
    	'State',
    ];
}
