<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/produccion'], function (Router $router) {
    $router->bind('produccion', function ($id) {
        return app('Modules\Produccion\Repositories\ProduccionRepository')->find($id);
    });
    $router->get('produccions', [
        'as' => 'admin.produccion.produccion.index',
        'uses' => 'ProduccionController@index',
        'middleware' => 'can:produccion.produccions.index'
    ]);

    $router->post('produccions/index_ajax', [
        'as' => 'admin.produccion.produccion.index_ajax',
        'uses' => 'ProduccionController@index_ajax',
        'middleware' => 'can:produccion.produccions.index_ajax'
    ]);
    
    $router->get('produccions/create', [
        'as' => 'admin.produccion.produccion.create',
        'uses' => 'ProduccionController@create',
        'middleware' => 'can:produccion.produccions.create'
    ]);
    $router->post('produccions', [
        'as' => 'admin.produccion.produccion.store',
        'uses' => 'ProduccionController@store',
        'middleware' => 'can:produccion.produccions.store'
    ]);
    $router->get('produccions/{produccion}/edit', [
        'as' => 'admin.produccion.produccion.edit',
        'uses' => 'ProduccionController@edit',
        'middleware' => 'can:produccion.produccions.edit'
    ]);
    $router->put('produccions/{produccion}', [
        'as' => 'admin.produccion.produccion.update',
        'uses' => 'ProduccionController@update',
        'middleware' => 'can:produccion.produccions.update'
    ]);
    $router->delete('produccions/{produccion}', [
        'as' => 'admin.produccion.produccion.destroy',
        'uses' => 'ProduccionController@destroy',
        'middleware' => 'can:produccion.produccions.destroy'
    ]);
     $router->get('produccions/index_cliente', [
        'as' => 'admin.produccion.produccion.index_cliente',
        'uses' => 'ProduccionController@index_cliente',
        'middleware' => 'can:produccion.produccions.index_cliente'
    ]);
     $router->post('produccions/actualizar_produccion', [
        'as' => 'admin.produccion.produccion.actualizar_produccion',
        'uses' => 'ProduccionController@actualizar_produccion',
        'middleware' => 'can:produccion.produccions.actualizar_produccion'
    ]);
     
    $router->post('produccions/actualizar_sugerido', [
        'as' => 'admin.produccion.produccion.actualizar_sugerido',
        'uses' => 'ProduccionController@actualizar_sugerido',
        'middleware' => 'can:produccion.produccions.actualizar_sugerido'
    ]); 
     




    $router->bind('entrega', function ($id) {
        return app('Modules\Produccion\Repositories\EntregaRepository')->find($id);
    });
    $router->get('entregas', [
        'as' => 'admin.produccion.entrega.index',
        'uses' => 'EntregaController@index',
        'middleware' => 'can:produccion.entregas.index'
    ]);

    $router->post('entregas/index_ajax', [
        'as' => 'admin.produccion.entrega.index_ajax',
        'uses' => 'EntregaController@index_ajax',
        'middleware' => 'can:produccion.entregas.index_ajax'
    ]);

    $router->post('entregas/reposicion_index', [
        'as' => 'admin.produccion.entrega.reposicion_index',
        'uses' => 'EntregaController@reposicion_index',
        'middleware' => 'can:produccion.entregas.reposicion_index'
    ]);

    $router->get('entregas/create', [
        'as' => 'admin.produccion.entrega.create',
        'uses' => 'EntregaController@create',
        'middleware' => 'can:produccion.entregas.create'
    ]);
    $router->post('entregas', [
        'as' => 'admin.produccion.entrega.store',
        'uses' => 'EntregaController@store',
        'middleware' => 'can:produccion.entregas.store'
    ]);
    $router->get('entregas/{entrega}/edit', [
        'as' => 'admin.produccion.entrega.edit',
        'uses' => 'EntregaController@edit',
        'middleware' => 'can:produccion.entregas.edit'
    ]);
    $router->put('entregas/{entrega}', [
        'as' => 'admin.produccion.entrega.update',
        'uses' => 'EntregaController@update',
        'middleware' => 'can:produccion.entregas.update'
    ]);
    $router->delete('entregas/{entrega}', [
        'as' => 'admin.produccion.entrega.destroy',
        'uses' => 'EntregaController@destroy',
        'middleware' => 'can:produccion.entregas.destroy'
    ]);
// append


});
