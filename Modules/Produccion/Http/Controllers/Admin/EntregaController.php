<?php namespace Modules\Produccion\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Produccion\Entities\Entrega;
use Modules\Produccion\Repositories\EntregaRepository;
use Yajra\Datatables\Facades\Datatables;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB,Session;

class EntregaController extends AdminBaseController
{
    /**
     * @var EntregaRepository
     */
    private $entrega;

    public function __construct(EntregaRepository $entrega)
    {
        parent::__construct();

        $this->entrega = $entrega;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()//Reposicion Index
    {
        return view('produccion::admin.reposicions.index', compact('entregas'));
    }

    public function index_ajax(Request $request)//Reposicion Index
    {
        // dd($request);
        $query = DB::table('cabecera')
                  ->join('Reposicion','Reposicion.COD_CONTACTO','=','cabecera.COD_CONTACTO')
                  ->groupBy('Reposicion.FECHA')
                  ->select([
                            "Reposicion.id as id",
                            "cabecera.NOMBRE as cliente",
                            "Reposicion.FECHA as fecha"
                        ]);

            $object = Datatables::of( $query )

            ->addColumn('action', function ($tabla) 
            {
                $asView = "admin.produccion.entrega.reposicion_index";
                
                $asView = route( $asView, [$tabla->id]);

                $buttons=" <div class='btn-group'>

                            <a href='".$asView." ' class='btn btn-primary btn-flat'>
                                <b>Ver Reposicion</b>
                            </a>
                            
                        </div>";

                return $buttons;
            })
    
            ->make(true);
            
        return $object;
    }
    public function reposicion_index() 
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('produccion::admin.entregas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->entrega->create($request->all());

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('produccion::entregas.title.entregas')]));

        return redirect()->route('admin.produccion.entrega.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Entrega $entrega
     * @return Response
     */
    public function edit(Entrega $entrega)
    {
        return view('produccion::admin.entregas.edit', compact('entrega'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Entrega $entrega
     * @param  Request $request
     * @return Response
     */
    public function update(Entrega $entrega, Request $request)
    {
        $this->entrega->update($entrega, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('produccion::entregas.title.entregas')]));

        return redirect()->route('admin.produccion.entrega.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Entrega $entrega
     * @return Response
     */
    public function destroy(Entrega $entrega)
    {
        $this->entrega->destroy($entrega);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('produccion::entregas.title.entregas')]));

        return redirect()->route('admin.produccion.entrega.index');
    }
}
