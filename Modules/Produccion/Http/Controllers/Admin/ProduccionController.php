<?php namespace Modules\Produccion\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Produccion\Entities\Produccion;
use Modules\Productos\Entities\Producto;
use Modules\User\Entities\Sentinel\User;
use Yajra\Datatables\Facades\Datatables;
use Modules\Produccion\Repositories\ProduccionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Session,DB, DateTime;

class ProduccionController extends AdminBaseController
{
    /**
     * @var ProduccionRepository
     */
    private $produccion;

    public function __construct(ProduccionRepository $produccion)
    {
        parent::__construct();
        $this->produccion = $produccion;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$produccions = $this->produccion->all();

        return view('produccion::admin.produccions.index', compact(''));
    }

    public function index_cliente(Request $request)
    {
        $sugerido_idem = $request["idem"];
 
        $query = DB::table('Sugerido')->where("idem",$sugerido_idem)->first();

        $factura = DB::table("cliente_factura")
                 ->join("Sugerido","Sugerido.idem","=","cliente_factura.reposicion_idem")
                 ->where("Sugerido.idem",$query->idem)
                 ->first();
        // dd($factura);

        $cliente_id = $query->COD_CONTACTO; 
        $fecha = $query->FECHA; 
        
        $datos = Produccion::join("cabecera","cabecera.COD_CONTACTO","=","Sugerido.COD_CONTACTO")
               ->join("Productos","Productos.COD_PRODUCTO","=","Sugerido.COD_PRODUCTO")
               ->where("Sugerido.idem",$sugerido_idem)
               ->select([
                            "Sugerido.*",
                            "cabecera.NOMBRE",
                            "Productos.EMPRESA"
                        ])
               ->get();
        if ($factura) 
        {   
            // dd($factura);
            $lote = $factura->nro_lote;
            if ($lote)
            {
                $factura = $factura->nro_factura ;
                return view('produccion::admin.produccions.index_cliente', compact('datos','factura','lote'));
            }

            $factura = $factura->nro_factura ;
            
            return view('produccion::admin.produccions.index_cliente', compact('datos','factura','lote'));
            
        }
        else
        {   
            return view('produccion::admin.produccions.index_cliente', compact('datos'));
        }

    }

    public function actualizar_sugerido(Request $request) // Actualiza cabecera de Reposicion
    {
        $query = DB::table("cliente_factura")->where("reposicion_idem", $request["reposicion_idem"])->first();
       
        if ($query)
        {
            // dd("update");
            DB::table("cliente_factura")
              ->where("reposicion_idem", $request["reposicion_idem"])
              ->update([
                    "nro_factura" => $request["nro_factura"],
                ]);
        }
        else
        {
            // dd($request["reposicion_idem"]);
            DB::table("cliente_factura")
               ->insert(
                [
                    "nro_factura" => $request["nro_factura"],
                    "reposicion_idem" => $request["reposicion_idem"],
                ]);
        }
    }   

    public function actualizar_produccion(Request $request)
    {   
        // dd($request->all());
        $object = json_encode($request->all(), true);
        $user = Session::get('user_mail');
        $name = User::where('email',$user)->first()->first_name;
        $last_name = User::where('email',$user)->first()->last_name;
        $usuario = $name.$last_name;
        $sugerido_info = Produccion::where("id",$request["table_id"])->first();
        $sabores = $request["sabor"];
        $lotes = $request["lotes"];
        $pesos = $request["pesos"];
        $date = date('Y-m-d');
        $datetime = new DateTime($date);
        $datetime->modify('+1 day');
        $fecha_entrega = $datetime->format('Y-m-d');

        $producto_reposicion =  DB::table("producto_reposicion")
                ->insert([
                            "producto_obj" => $object,
                            "idem" =>$sugerido_info->idem,
                        ]); 

        if ($request["is4d"] == true)
        {   
            $peso=0;
            $cantidad=0;
            foreach ($pesos as $key => $value) 
            {
                $peso = $peso + $value;
                $cantidad = $cantidad +1;
            }  

            $reposicion = DB::table("Reposicion")
                ->insert([
                   "COD_CONTACTO" => $sugerido_info->COD_CONTACTO ,
                   "COD_PRODUCTO" => $sugerido_info->COD_PRODUCTO ,
                   "DESCRIPCION" => $sugerido_info->DESCRIPCION,
                   "LOTE" => $lotes[0],
                   "idem" => $sugerido_info->idem,
                   "id_pro" => "0",
                   "syncm" => "0",
                   "State" => "0",
                   "idem_frigo" => "0",
                   "CANTIDAD" => $cantidad,
                   "UNIDAD" =>  $peso,
                   "FECHA" => $date,
                   "user" => $usuario,
                   "id_ser" => $sugerido_info->idem.$sugerido_info->COD_PRODUCTO,
                   "forup" => "0",
                   "fecha_entrega"=> $fecha_entrega,
                ]); 
            
            Produccion::where("id",$request["table_id"])->update(["Producido" => true]); 
        }
        else if ($request["isPaleto"]==true) 
        {
            $cantidad = $request["producido"];
            $reposicion = DB::table("Reposicion")
                ->insert([
                   "COD_CONTACTO" => $sugerido_info->COD_CONTACTO ,
                   "COD_PRODUCTO" => $sugerido_info->COD_PRODUCTO ,
                   "DESCRIPCION" => $sugerido_info->DESCRIPCION,
                   "LOTE" => $request["lote"],
                   "idem" => $sugerido_info->idem,
                   "id_pro" => "0",
                   "syncm" => "0",
                   "State" => "0",
                   "idem_frigo" => "0",
                   "CANTIDAD" => $cantidad,
                   "UNIDAD" =>  null,
                   "FECHA" => $date,
                   "user" => $usuario,
                   "id_ser" => $sugerido_info->idem.$sugerido_info->COD_PRODUCTO,
                   "forup" => "0",
                   "fecha_entrega"=> $fecha_entrega,
                ]); 
            Produccion::where("id",$request["table_id"])->update(["Producido" => true]); 
        }
        else
        {
            Produccion::where("id",$sugerido_info->id)->update(["Producido" => false]);
            $produccion = Produccion::where("id",$table_id)->first();
            // $produccion->update()

            DB::table("Reposicion")->where("COD_CONTACTO",$produccion->COD_CONTACTO)
                                   ->where("COD_PRODUCTO",$produccion->COD_PRODUCTO)
                                   ->where("FECHA",$produccion->FECHA)
                                   ->delete();
        }
    }    

    public function index_ajax(Request $request)
    {
        // dd($request->all());
        $query = Produccion::join("cabecera","cabecera.COD_CONTACTO","=","Sugerido.COD_CONTACTO")
                           ->where("Sector","Produccion")
                           ->select([
                                "Sugerido.idem as idem",
                                "Sugerido.FECHA as fecha",
                                "Sugerido.*",
                                "cabecera.NOMBRE"
                                    ])
                           ->orderBy("fecha","DESC")   
                           ->groupBy("Sugerido.idem")
                           ->get();

        $object = Datatables::of( $query )
            ->addColumn('action', function ($tabla) 
            {
                $asView = "admin.produccion.produccion.index_cliente";
                
                $asView = route( $asView, ["idem" => $tabla->idem]);

                $buttons=" <div class='btn-group'>

                            <a href='".$asView." ' class='btn btn-primary btn-flat'>
                                <b>Ver Sugerido</b>
                            </a>
                            
                        </div>";

                return $buttons;

            })

            ->editColumn('Producido', function($tabla)
            {   
                $check = asset('assets/media/check.png');
                $error = asset('assets/media/error.png');

                $producidos = Produccion::where("idem",$tabla->idem)
                              ->where("CANTIDAD","<>",0)
                              ->get();     
                $aux = true;

                foreach ($producidos as $key => $producido)
                {
                    if($producido->Producido == 0)
                    {
                        $aux = false;
                    }
                }

                if ($aux == false)
                {
                    return "<img src=".$error." style='width: 40px; height: 40px'>";
                }
                else
                {
                    return "<img src=".$check." style='width: 40px; height: 40px'>";
                } 
            })
            
            ->filter(function ($query) use ($request)
            {
                if ($request->has('DESCRIPCION')  && trim($request->has('DESCRIPCION') !== '') )
                {
                    $query->where('Sugerido.DESCRIPCION', 'like', "%{$request->get('DESCRIPCION')}%");
                }
            })

            ->make(true);
            
        return $object;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $productos = Producto::where("activo",true)->lists("DESCRIPCION","COD_PRODUCTO")->all();
        setlocale(LC_ALL,Null);
        $fecha = date("d-m-Y");
        
        return view('produccion::admin.produccions.create' , compact('productos','fecha'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return redirect()->route('admin.produccion.produccion.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Produccion $produccion
     * @return Response
     */
    public function edit(Produccion $produccion)
    {
        $fecha = date("Y-m-d");
        $productos = Producto::where("activo",true)->lists("DESCRIPCION","COD_PRODUCTO")->all();

        return view('produccion::admin.produccions.edit', compact('produccion','fecha','productos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Produccion $produccion
     * @param  Request $request
     * @return Response
     */
    public function update(Produccion $produccion, Request $request)
    {
        $this->produccion->update($produccion, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('produccion::produccions.title.produccions')]));

        return redirect()->route('admin.produccion.produccion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Produccion $produccion
     * @return Response
     */
    public function destroy(Produccion $produccion)
    {
        $this->produccion->destroy($produccion);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('produccion::produccions.title.produccions')]));

        return redirect()->route('admin.produccion.produccion.index');
    }
}
