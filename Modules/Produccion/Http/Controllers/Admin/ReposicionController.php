<?php namespace Modules\Produccion\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Produccion\Entities\Produccion;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB,Session;

class ReposicionController extends AdminBaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$reposicions = $this->reposicion->all();

        return view('produccion::admin.reposicions.index', compact(''));
    }

}
