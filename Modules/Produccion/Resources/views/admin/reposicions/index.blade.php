@extends('layouts.master')

@section('content-header')
@stop



@section('content')
    <br>
     <h1>
        Reposicion
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('produccion::entregas.title.entregas') }}</li>
    </ol>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
               {{--  <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.produccion.entrega.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('produccion::entregas.button.create entrega') }}
                    </a>
                </div> --}}
            </div>
            <div class="box box-primary">

                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="data-table table table-bordered table-hover">
                        <thead>
                            {{-- <th>id</th> --}}
                            <th>Clientes</th>
                            <th>Fecha</th>
                            {{-- <th>Reposicion Completa</th> --}}
                            <th>Acciones</th>

                        </thead>
                        <tbody>

                        <tr>
                            {{-- <td></td> --}}
                            <td></td>
                            <td></td>
                            {{-- <td></td> --}}
                            <td></td>
                        </tr>

                        </tbody>
                        <tfoot>
                            {{-- <th>id</th> --}}
                            <th>Clientes</th>
                            <th>Fecha</th>
                            {{-- <th>Reposicion Completa</th> --}}
                            <th>Acciones</th>


                        </tfoot>
                    </table>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('produccion::entregas.title.create entrega') }}</dd>
    </dl>
@stop

@section('scripts')
    <?php $locale = locale(); ?>
    <script type="text/javascript">

            var table = $('.data-table').DataTable(
            {
                dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                "<'row'<'col-xs-12't>>"+
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                "deferRender": true,
                processing: false,
                serverSide: true,
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ;?>'
                },
                ajax: 
                 {
                    url: '{!! route('admin.produccion.entrega.index_ajax') !!}',
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: function (e) 
                    {
                        e.categoria = $('#categoria').val();
                    }
                },
                columns: 
                [
                    // { data: 'id', name: 'id' },
                    { data: 'cliente' , name: 'cliente' },
                    { data: 'fecha' , name: 'fecha' },
                    // { data: 'reposicion' , name: 'reposicion' },
                    { data: 'action' , name: 'action' }
                ]
            });

    </script>
@stop
