@extends('layouts.master')

@section('content-header')
    
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('produccion::produccions.title.produccions') }}</li>
    </ol>
@stop

@section('styles')
    {!! Theme::script('js/vendor/ckeditor/ckeditor.js') !!}
    <style type="text/css">
        th 
        {
            background-color: #f6f6f6;
            /*color: #fff;*/
        }
    </style>
@stop

@section('content')
    <br>
    <h1>
        Sugerido
    </h1>
    <input type="hidden" name="fecha" id="fecha" value="{{$datos[0]->FECHA}}">

    <h4><b>Cliente : </b>{{$datos[0]->NOMBRE}}</h4>
    <div class="row">
        <div class="col-xs-12">
                <br>
                <br>
            <div class="box box-primary">
              {{--   {!! Form::open(['route' => ['admin.produccion.produccion.actualizar_sugerido'], 'method' => 'post']) !!} --}}
    
                <div class="box-header">
                    <div class="btn-group col-md-3" style="margin: 0 15px 15px 0;">
                    <label class="mylabel"> Nro Factura:</label>
                    @if(isset($factura))
                        <table>
                            <tr>
                                <td><input class="form-control" type="text" name="nro_factura" id="nro_factura" value="{{$factura}}"></td>
                                <td><a class="btn btn-primary btn-flat confirmar" id="confirmar_factura">Confirmar</a></td>
                            </tr>
                        </table>
                    @else
                        <table>
                            <tr>
                                <td><input class="form-control" type="text" name="nro_factura" id="nro_factura" ></td>
                                <td><a class="btn btn-primary btn-flat confirmar" id="confirmar_factura">Confirmar</a></td>
                            </tr>
                        </table>                        
                    @endif
                    <input type="hidden" name="factura_idem" id="factura_idem" value="{{$datos[0]->idem}}">
                </div>
                </div>
                <!-- /.box-header -->

            </div>
                    <div class="table-responsive">
                    @if(isset($factura))
                        <table class="data-table table table-bordered table-hover" id="tablaProduccionItem" >
                    @else
                        <table class="data-table table table-bordered table-hover" id="tablaProduccionItem" style="display: none">
                    @endif
                            <thead>
                                <tr>
                                    {{-- <th>Id</th> --}}
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Envase</th>
                                    <th>Producido</th>
                                    <th>Terminar</th>
                                </tr>   
                            </thead>
                            <tbody> 
                            
                            @foreach($datos as $key => $value)
                                <tr>
                                    <td id="id" style="display: none;"> {{$value->id}}</td>
                                    <td><label class="mylabel">{{$value->DESCRIPCION}}</label></td>

                                    <td name="cantidad" id="{{$value->CANTIDAD}}">
                                        <a type="text" style="color: #000" value="{{$value->CANTIDAD}}">{{$value->CANTIDAD}}</a>
                                    </td>  

                                    <td>
                                        {{$value->ENVASE}}
                                    </td>

                                    <td>
                                        @if($value->Producido ==0)
                                            @if($value->EMPRESA =="Paletto")
                                                <img class="paletto" src="{{asset('assets/media/error.png')}}" style="width: 40px; height: 40px">
                                            @else
                                                <img class="4d" src="{{asset('assets/media/error.png')}}" style="width: 40px; height: 40px">
                                            @endif
                                        @else
                                            <img src="{{asset('assets/media/check.png')}}" style="width: 40px; height: 40px">
                                        @endif
                                    </td>


                                    <td id="estado">
                                        @if($value->Producido == 0)
                                            <input name="activo[]" type="checkbox" class="icheckbox_flat-blue activo" >
                                        @else
                                            <input name="activo[]" type="checkbox" class="icheckbox_flat-blue activo" checked>
                                        @endif
                                    </td>
                                    {{-- <td>{{$value->}}</td> --}}
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                {{-- <th>Id</th> --}}
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Envase</th>
                                <th>Producido</th>
                                <th>Terminar</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <button class="btn btn-primary" type="button" id="produccion_paletto" data-toggle="modal" data-target="#produccion_paletto_modal" style="display: none;">Modal</button>
                     <button class="btn btn-primary" type="button" id="produccion_4D" data-toggle="modal" data-target="#produccion_4D_modal" style="display: none;">Modal</button>
                    <input type="text" name="array_terminados" id="array_terminados" style="display: none" > 
                    <!-- /.box-body --> 
                    <button type="button " id="guardar_cambios" class="btn btn-primary ">Guardar Cambios</button>
                </div>
                {{-- {!! Form::close() !!} --}}
                <!-- /.box -->
            </div>
        </div>
    </div>
    {{-- @include('core::partials.delete-modal') --}}
    @include('core::partials.produccion_paletto_modal')
    @include('core::partials.produccion_4D_modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('produccion::produccions.title.create produccion') }}</dd>
    </dl>
@stop

@section('scripts')

    
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $( document ).ready(function() 
        {  
                var array_terminados = new Array();
                $(".confirmar").on("click",function()
                {
                    var factura = $("#nro_factura").val();
                    var idem = $("#factura_idem").val();

                    $.ajax
                    ({
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                        url: '{!! route('admin.produccion.produccion.actualizar_sugerido') !!}',
                        type: 'POST',
                        data: {
                                nro_factura: factura, 
                                reposicion_idem: idem, 
                              },   
                        success: function()
                        {   
                            $("#tablaProduccionItem").show();
                        }
                    });
                });

            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $("#tablaProduccionItem tr td:nth-child(5)").each(function ()
            {
                // console.log($(this).closest("tr").find("td").html());
                var image = $(this).find("img").attr("src");
                var result = image.search("check.png");

                if (result != -1)
                {
                    var row_id = $(this).closest("tr").find("td").html();
                    array_terminados.push(row_id);
                    $("#array_terminados").val(array_terminados);
                }
            });

            $(".activo").change(function() 
            {
                var value = $(this).prop("checked");
                var id = $(this).closest("tr").find("td").html();
                var empresa = $(this).closest("tr").find("img").attr("class");
                var cantidad = $(this).closest("tr").find("a").html();
                var sabor = $(this).closest("tr").find("label").html();
                // alert(sabor);
                if(value != false) 
                {
                    if (empresa == "4d")
                    {
                        $("input[name=producto_id]").val(id);
                        $("#produccion_4D").click();
                        $("#table_id").val(id)
                        $("input[name=cantidad_producida]").val(cantidad);
                        $("input[name=sabor]").val(sabor);
                        array_terminados.push(id); 
                    }
                    else
                    {
                        $("input[name=producto_id]").val(id);
                        $("#produccion_paletto").click();
                        $("#table_id").val(id)
                        $("input[name=producido]").val(cantidad);
                        array_terminados.push(id);
                    }
                }
                else
                {
                    $("input[name=producto_id]").val(id);
                    // $("#produccion_delete").click();
                    $("#table_id").val(id)
            
                    var indice = array_terminados.indexOf(id);
                    array_terminados.splice(indice , 1);
                    var producido = $("input[name=producido]").val();       
           
                    $.ajax
                    ({
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                        url: '{!! route('admin.produccion.produccion.actualizar_produccion') !!}',
                        type: 'POST',
                        data: {
                                table_id: id, 
                              },   
                        success: function()
                        {   
                            $.alert
                            ({
                                title: 'Eliminado',
                                content: 'Se ha Eliminado con Exito',
                            });
                        }
                    });
                    location.reload();
                }
            });           
        });

      
    </script>
@stop
