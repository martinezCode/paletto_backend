@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('produccion::produccions.title.produccions') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('produccion::produccions.title.produccions') }}</li>
    </ol>
@stop

@section('styles')
<style type="text/css">
    th 
        {
            background-color: #f6f6f6;
            /*color: #fff;*/
        }
</style>
@stop   

@section('content')
<h1>
        {{ trans('produccion::produccions.title.produccions') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('produccion::produccions.title.produccions') }}</li>
    </ol>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                  {{--   <a href="{{ route('admin.produccion.produccion.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> Nueva Produccion
                    </a> --}}
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="tablaProduccion">
                            <thead>
                                <tr>
                                    <th style="display: none">id</th>
                                    <th>Cliente</th>
                                    <th>Fecha</th>
                                    <th>Produccion Terminada</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody> 
                                <tr>
                                    <td style="display: none"></td>
                                    <td></td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th style="display: none">id</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Produccion Terminada</th>
                                <th>Acciones</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('produccion::produccions.title.create produccion') }}</dd>
    </dl>
@stop

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.produccion.produccion.create') ;?>" }
                ]
            });
        });
    </script>

    <?php $locale = locale(); ?>
    <script type="text/javascript">
        var table = $('.data-table').DataTable(
            {
                dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                "<'row'<'col-xs-12't>>"+
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                "deferRender": true,
                processing: false,
                serverSide: true,
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ;?>'
                },
                ajax: 
                 {
                    url: '{!! route('admin.produccion.produccion.index_ajax') !!}',
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: function (e) 
                    {
                        // e.categoria = $('#categoria').val();
                    }
                },
                columns: 
                [
                    { data: 'idem', name: 'idem', visible:false },
                    { data: 'NOMBRE', name: 'NOMBRE' },
                    { data: 'FECHA' , name: 'FECHA'},
                    { data: 'Producido' , name: 'Producido'},
                    { data: 'action', name: 'action', orderable: false, searchable: false} 
                ]
            });
    </script>
@stop
