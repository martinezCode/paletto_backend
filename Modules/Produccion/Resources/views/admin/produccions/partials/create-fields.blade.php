<div class="box-body">
    <p>
        {!! Form::normalInput('fecha', 'Fecha', $errors , (object)['fecha' => $fecha],['readonly' => '']) !!}
        {!! Form::normalSelect('COD_PRODUCTO', 'Producto', $errors ,$productos) !!}
        {!! Form::normalInput('CANTIDAD', 'Cantidad', $errors) !!}
		{{-- {!! Form::normalCheckbox('ACTIVO', 'Activo', $errors) !!} --}}
    </p>
</div>
{!! Theme::script('js/jquery.number.min.js') !!}
<script type="text/javascript">

	$("input[name=CANTIDAD]").number(true, 0, '', '.');

</script>
