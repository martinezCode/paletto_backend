<?php

return [
    'title' => [
        'entregas' => 'Entrega',
        'create entrega' => 'Create a entrega',
        'edit entrega' => 'Edit a entrega',
    ],
    'button' => [
        'create entrega' => 'Create a entrega',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
