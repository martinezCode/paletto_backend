<?php

return [
    'title' => [
        'produccions' => 'Produccion',
        'create produccion' => 'Create a produccion',
        'edit produccion' => 'Edit a produccion',
    ],
    'button' => [
        'create produccion' => 'Create a produccion',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
