<?php namespace Modules\Produccion\Repositories\Eloquent;

use Modules\Produccion\Repositories\EntregaRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentEntregaRepository extends EloquentBaseRepository implements EntregaRepository
{
}
