<?php namespace Modules\Produccion\Repositories\Eloquent;

use Modules\Produccion\Repositories\ProduccionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProduccionRepository extends EloquentBaseRepository implements ProduccionRepository
{
}
