<?php namespace Modules\Produccion\Repositories\Cache;

use Modules\Produccion\Repositories\ProduccionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheProduccionDecorator extends BaseCacheDecorator implements ProduccionRepository
{
    public function __construct(ProduccionRepository $produccion)
    {
        parent::__construct();
        $this->entityName = 'produccion.produccions';
        $this->repository = $produccion;
    }
}
