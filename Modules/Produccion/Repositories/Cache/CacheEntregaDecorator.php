<?php namespace Modules\Produccion\Repositories\Cache;

use Modules\Produccion\Repositories\EntregaRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheEntregaDecorator extends BaseCacheDecorator implements EntregaRepository
{
    public function __construct(EntregaRepository $entrega)
    {
        parent::__construct();
        $this->entityName = 'produccion.entregas';
        $this->repository = $entrega;
    }
}
