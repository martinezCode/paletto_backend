<?php namespace Modules\Produccion\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item("Producciones", function (Item $item) {
                $item->icon('fa fa-cube');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item("Produccion", function (Item $item) {
                    $item->icon('fa fa-pencil-square-o');
                    $item->weight(0);
                    // $item->append('admin.produccion.produccion.create');
                    $item->route('admin.produccion.produccion.index');
                    $item->authorize(
                        $this->auth->hasAccess('produccion.produccions.index')
                    );
                });
                // $item->item("Reposicion", function (Item $item) {
                //     $item->icon('fa fa-cart-arrow-down');
                //     $item->weight(0);
                //     // $item->append('admin.produccion.entrega.create');
                //     $item->route('admin.produccion.entrega.index');
                //     $item->authorize(
                //         $this->auth->hasAccess('produccion.entregas.index')
                //     );
                // });
// append
            });
        });

        return $menu;
    }
}
