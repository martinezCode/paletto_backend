<?php namespace Modules\Produccion\Providers;

use Illuminate\Support\ServiceProvider;

class ProduccionServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Produccion\Repositories\ProduccionRepository',
            function () {
                $repository = new \Modules\Produccion\Repositories\Eloquent\EloquentProduccionRepository(new \Modules\Produccion\Entities\Produccion());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Produccion\Repositories\Cache\CacheProduccionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Produccion\Repositories\EntregaRepository',
            function () {
                $repository = new \Modules\Produccion\Repositories\Eloquent\EloquentEntregaRepository(new \Modules\Produccion\Entities\Entrega());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Produccion\Repositories\Cache\CacheEntregaDecorator($repository);
            }
        );
// add bindings


    }
}
