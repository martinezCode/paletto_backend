<?php namespace Modules\Frigos\Repositories\Eloquent;

use Modules\Frigos\Repositories\FrigoConfigRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentFrigoConfigRepository extends EloquentBaseRepository implements FrigoConfigRepository
{
}
