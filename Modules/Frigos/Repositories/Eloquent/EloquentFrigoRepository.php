<?php namespace Modules\Frigos\Repositories\Eloquent;

use Modules\Frigos\Repositories\FrigoRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentFrigoRepository extends EloquentBaseRepository implements FrigoRepository
{
}
