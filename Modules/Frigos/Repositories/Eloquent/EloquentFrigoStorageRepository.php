<?php namespace Modules\Frigos\Repositories\Eloquent;

use Modules\Frigos\Repositories\FrigoStorageRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentFrigoStorageRepository extends EloquentBaseRepository implements FrigoStorageRepository
{
}
