<?php namespace Modules\Frigos\Repositories\Cache;

use Modules\Frigos\Repositories\FrigoRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheFrigoDecorator extends BaseCacheDecorator implements FrigoRepository
{
    public function __construct(FrigoRepository $frigo)
    {
        parent::__construct();
        $this->entityName = 'frigos.frigos';
        $this->repository = $frigo;
    }
}
