<?php namespace Modules\Frigos\Repositories\Cache;

use Modules\Frigos\Repositories\FrigoStorageRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheFrigoStorageDecorator extends BaseCacheDecorator implements FrigoStorageRepository
{
    public function __construct(FrigoStorageRepository $frigostorage)
    {
        parent::__construct();
        $this->entityName = 'frigos.frigostorages';
        $this->repository = $frigostorage;
    }
}
