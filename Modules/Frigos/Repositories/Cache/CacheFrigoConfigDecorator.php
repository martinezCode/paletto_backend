<?php namespace Modules\Frigos\Repositories\Cache;

use Modules\Frigos\Repositories\FrigoConfigRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheFrigoConfigDecorator extends BaseCacheDecorator implements FrigoConfigRepository
{
    public function __construct(FrigoConfigRepository $frigoconfig)
    {
        parent::__construct();
        $this->entityName = 'frigos.frigoconfigs';
        $this->repository = $frigoconfig;
    }
}
