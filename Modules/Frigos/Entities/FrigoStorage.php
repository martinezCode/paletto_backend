<?php namespace Modules\Frigos\Entities;

use Illuminate\Database\Eloquent\Model;

class FrigoStorage extends Model
{
    protected $table = 'Stock_Frigo_Config_Storage_Prod';

    protected $fillable = 
    [
    	"COD_FRIGO",
    	"COD_CONFIG",
        "COD_PRODUCTO",
        "COD_GRAN_STORAGE",
    	"COD_STORAGE",
    	"DESCRIPCION",
    	"STOCK_MIN",
    	"STOCK_MAX",
    	"idem",
    	"user"
    ];
}
