<?php namespace Modules\Frigos\Entities;


use Illuminate\Database\Eloquent\Model;

class Frigo extends Model
{
    protected $table = 'Frigos';

    protected $fillable = 
    [
    	"DESCRIPCION",
    	"COD_FRIGO",
    	"BANDEJAS",
    	"CAJAS",
    	"USER",	
    ];
}
