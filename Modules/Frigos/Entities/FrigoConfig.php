<?php namespace Modules\Frigos\Entities;


use Illuminate\Database\Eloquent\Model;

class FrigoConfig extends Model
{
    protected $table = 'Frigo_Config';

    protected $fillable = 
    [
    	"COD_FRIGO",
    	"COD_CONFIG",
    	"DESCRIPCION",
    	"idem",
    	"user"
    ];
}
