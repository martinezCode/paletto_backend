<?php

return [
    'frigos.frigos' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
    'frigos.frigoconfigs' => [
        'index',
        'index_ajax',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
    'frigos.frigostorages' => [
        'index',
        'index_ajax',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
        'sabores_disponibles',
        'contenido_item',
        'storage',
        'store_item_content',
        'borrar_item',

    ],
// append



];
