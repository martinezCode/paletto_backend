@extends('layouts.master')

@section('content-header')
    <h1>
        Almacenamiento de Frigos
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ "Almacenamiento Frigos" }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                {{-- <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.frigos.frigostorage.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ "Crear Almacenamiento" }}
                    </a>
                </div> --}}
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="tablaFrigos" >
                            <thead>
                            <tr>
                                <th style="display: none;">id</th>
                                <th class="col-md-4" >Frigo</th>
                                <th class="col-md-4">Codigo de Frigo</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                            </tbody>
                            <tfoot>
                            <tr>
                                <th style="display: none;">id</th>
                                <th>Frigo</th>
                                <th>Codigo de Frigo</th>
                                <th>Acciones</th>
                            </tr>

                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('frigos::frigostorages.title.create frigostorage') }}</dd>
    </dl>
@stop

@section('scripts')
    <?php $locale = locale(); ?>
    <script type="text/javascript">


        $(document).ready(function()
        {
            $("input[name=nombre_frigo]").keyup(function()
            {
                $("#search-form").submit();
            });

            $("#codigo_frigo").on("keyup",function()
            {
                $("#search-form").submit();
            });

            $("#descripcion").on("keyup",function()
            {
                $("#search-form").submit();
            });

            $('#search-form').submit(function(e) 
            {
                table.draw();
                e.preventDefault();
            });

            var table = $('.data-table').DataTable(
            {
                dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                "<'row'<'col-xs-12't>>"+
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                "deferRender": true,
                'responsive': true,
                processing: false,
                serverSide: true,
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": false,
                "paginate": true,
                 "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ;?>'
                },
                ajax: 
                 {
                    url: '{!! route('admin.frigos.frigostorage.index_ajax') !!}',
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: function (e) 
                    {
                        e.nombre_frigo = $('#nombre_frigo').val();                
                    }
                },
                columns: 
                [
                    { data: 'id', name: 'id' , visible: false },
                    { data: 'nombre_frigo', name: 'nombre_frigo' },
                    { data: 'codigo_frigo', name: 'codigo_frigo' },
                    { data: 'acciones', name: 'acciones', orderable: false, searchable: false} 
                ], 
            });
        });
    </script>
@stop
