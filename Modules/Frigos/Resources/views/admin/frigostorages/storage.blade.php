@extends('layouts.master')

@section('content-header')
    <h1>
       Almacenamiento de Frigo
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.frigos.frigo.index') }}">{{ trans('frigos::frigos.title.frigos') }}</a></li>
        <li class="active">{{ trans('frigos::frigos.title.edit frigo') }}</li>
    </ol>
@stop

@section('styles')
    {!! Theme::script('js/vendor/ckeditor/ckeditor.js') !!}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
       #gallery { float: left; width: 90%; min-height: auto; }
          .gallery.custom-state-active { background: #eee; }
<<<<<<< HEAD
          .gallery li { float: left; width: 100px; height: : 120px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
          .gallery li h5 { margin: 0 0 0.4em; cursor: move; background-color: #4d667b; color:#fff;height: 32px}
=======
          .gallery li { float: left; width: 102px; height: : 161px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
          .gallery li h5 { margin: 0 0 0.4em; cursor: move; background-color: rgba(228, 89, 90, 0.64); color:#fff;height: 32px}
>>>>>>> 723e2cc45c50244d3d09a9e4a20c8153b15e792d
          .gallery li a { float: right; }
          .gallery li a.ui-icon-zoomin { float: left; }
          .gallery li span { width: 100%; cursor: move; }
          .gallery img { 
            border-style: solid;
            border-width: 1px;
            border-color: #c5c5c5;
          }
         
<<<<<<< HEAD
          #trash { position: center; width: 95%; min-height: 50em; height: auto; padding: 3%; }
          #trash h4 { line-height: 25px; margin: 0 0 0.4em; background-color: #4d667b; color:#fff;}
          #trash h4 .ui-icon { float: left; } 
          #trash .gallery h5 { display: noe; background-color: #4d667b; color:#fff; min-widthheight: 32px; text-align: center; vertical-align: 0px;}
=======
          #trash { position: center; width: 98%; min-height: 40em; height: auto; padding: 3%; }
          #trash h4 { line-height: 25px; margin: 0 0 0.4em; background-color: #e4595a; color:#fff;}
          #trash h4 .ui-icon { float: left; }
          #trash .gallery h5 { display: noe; background-color: rgba(228, 89, 90, 0.64); color:#fff; min-widthheight: 32px; text-align: center; vertical-align: 0px;}
>>>>>>> 723e2cc45c50244d3d09a9e4a20c8153b15e792d
          #trash .gallery b { font-size: 13px; }
          #trash li.ui-icon {position: absolute;  }

  </style>
@stop

@section('content')
    {{-- {!! Form::open(['route' => ['admin.frigos.storage-frigo.store_item_content'], 'method' => 'post']) !!} --}}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('frigos::admin.frigostorages.partials.storage-fields', ['lang' => $locale])

                           {{--  <input type="hiddn" placeholder="codigo de producto" name="codigo_producto" value="0">
                            <input type="hidden" name="storage" value="0">  --}}
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">Aceptar</button>
                        {{-- <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button> --}}
                        <a class="btn btn-default pull-right " href="{{ route('admin.frigos.frigostorage.index')}}"><i class="fa fa-times"></i><b> {{ trans('core::core.button.cancel') }}</b></a>
                        <button class="btn btn-primary btn-flat input-sm" type="button" id="stock-save" data-toggle="modal" data-target="#save_modal" style="display: none;">
                          Save_modal 
                        </button>
                    </div>
                    @include('core::partials.stock-save-modal')
                    {{-- @include('core::partials.stock-delete-modal') --}}
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {{-- {!! Form::close() !!} --}}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop

@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@section('scripts')
    {{-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
      
        $( document ).ready(function() 
        {    
            var codigo_frigo =  $('input[name=COD_FRIGO]').val();
            var configuracion =  $('select[name=CONFIGURACION_FRIGO]').val();
            

//---------------------------------------Al cargar la pagina--------------------------------------------------------//

//-----------------------------Ajax que carga los sabores ya contenidos---------------------------------------------//
                $.ajax
                ({
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    url: '{!! route('admin.frigos.frigostorage.contenido_item') !!}',
                    type: 'POST',
                    data: {
                            codigo_frigo: codigo_frigo, 
                            codigo_configuracion: configuracion
                          }, 
                    dataType: 'json',    
                    success: function( contenidos )
                    {   
                        // var esto_quite_del_foreach = "<a title="Quitar Sabor" class="ui-icon ui-icon-trash"></a>";
                        var recycle_icon = "<a title='Quitar Sabor' class='ui-icon ui-icon-trash'></a>"

                        $(".trash_galery").empty();
                        contenidos.forEach(function(element)
                        {
                          var stock = element.STOCK_MAX;
                          var stock_label = "<b>Sugerido: </b><label class='text-left' style='color:#ec4742;'> "+stock+"</label>";
                          var imagy_footer ="<table><tr><td>"+stock_label+"</td><td> "+recycle_icon+"</td></tr></table>";

                            $("#trash_galery").append(' <li class="ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle" id="'+element.COD_PRODUCTO+'" style="height: 150px"><h5 class="ui-widget-header">'+element.DESCRIPCION+'</h5><span class="bandeja" value="Bandeja " id="'+element.COD_PRODUCTO+'"><img src="'+element.archivo+'" style="width:80px ; height: 70px;"></span>'+imagy_footer+'</li>');
                        });

                        var uAg = navigator.userAgent.toLowerCase();
                        var isMobile = !!uAg.match(/android|iphone|ipad|ipod|blackberry|symbianos/i);

                        if (!isMobile)
                        {
                          @include('frigos::admin.frigostorages.scriptDragDrop', ['lang' => $locale])                               
                        }
                        else
                        {
                          @include('frigos::admin.frigostorages.scriptMovil', ['lang' => $locale])                               
                        }
                      }
                  });


  //--------------------------- Ajax que carga los sabores disponibles ------------------------------------------ //
         

                  $.ajax
                  ({
                      headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                      url: '{!! route('admin.frigos.frigostorage.sabores_disponibles') !!}',
                      type: 'POST',
                      data: {
                              codigo_frigo: codigo_frigo, 
                              codigo_configuracion: configuracion
                            }, 
                      dataType: 'json',    
                      success: function( disponibles )
                      {   

                        disponibles.forEach(function(item)
                        {
                            $(".sabor_gallery").append(' <li class="ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle" id="'+item.COD_PRODUCTO+'" style="height: 120px"><h5 class="ui-widget-header">'+item.DESCRIPCION+'</h5><span class="bandeja" value="" id="'+item.COD_PRODUCTO+'"><img src="'+item.archivo+'" style="width:80px ; height: 70px;">  </span> </span></li> ');
                             
                        });

                        var uAg = navigator.userAgent.toLowerCase();
                        var isMobile = !!uAg.match(/android|iphone|ipad|ipod|blackberry|symbianos/i);

                        if (!isMobile)
                        {
                          @include('frigos::admin.frigostorages.scriptDragDrop', ['lang' => $locale])                               
                        }
                        else
                        {
                          @include('frigos::admin.frigostorages.scriptMovil', ['lang' => $locale])                               
                        }
                      }
                  });
//---------------------------------------------------------------------------------------------------------------------------//


                    $('select[name=CONFIGURACION_FRIGO]').change(function()
                    { 

                        var codigo_frigo =  $('input[name=COD_FRIGO]').val();
                        var configuracion =  $('select[name=CONFIGURACION_FRIGO]').val();
  

  //-----------------------------Ajax que carga los sabores ya contenidos---------------------------------------------//

                        $.ajax
                        ({
                          headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                          url: '{!! route('admin.frigos.frigostorage.contenido_item') !!}',
                          type: 'POST',
                          data: {
                                  codigo_frigo: codigo_frigo, 
                                  codigo_configuracion: configuracion
                                }, 
                          dataType: 'json',    
                          success: function( contenidos )
                          {   
                            // var esto_quite_del_foreach = "<a title="Quitar Sabor" class="ui-icon ui-icon-trash"></a>";
                            var recycle_icon = "<a title='Quitar Sabor' class='ui-icon ui-icon-trash'></a>"

                            $(".trash_galery").empty();
                            contenidos.forEach(function(element)
                            {
                              var stock = element.STOCK_MAX;
                              var stock_label = "<b>Sugerido: </b><label class='text-left' style='color:#ec4742;'> "+stock+"</label>";
                              var imagy_footer ="<table><tr><td>"+stock_label+"</td><td> "+recycle_icon+"</td></tr></table>";

                                $("#trash_galery").append(' <li class="ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle" id="'+element.COD_PRODUCTO+'" style="height: 150px"><h5 class="ui-widget-header">'+element.DESCRIPCION+'</h5><span class="bandeja" value="Bandeja " id="'+element.COD_PRODUCTO+'"><img src="'+element.archivo+'" style="width:80px ; height: 70px;"></span>'+imagy_footer+'</li>');
                            });
                            var uAg = navigator.userAgent.toLowerCase();
                            var isMobile = !!uAg.match(/android|iphone|ipad|ipod|blackberry|symbianos/i);

                            if (!isMobile)
                            {
                              @include('frigos::admin.frigostorages.scriptDragDrop', ['lang' => $locale])                               
                            }
                            else
                            {
                              @include('frigos::admin.frigostorages.scriptMovil', ['lang' => $locale])                               
                            }
                      
                          }
                        });

            //--------------------------- Ajax que carga los sabores disponibles ------------------------------------------ //
                      $.ajax
                      ({
                          headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                          url: '{!! route('admin.frigos.frigostorage.sabores_disponibles') !!}',
                          type: 'POST',
                          data: {
                                  codigo_frigo: codigo_frigo, 
                                  codigo_configuracion: configuracion,
                                }, 
                          dataType: 'json',    
                          success: function( disponibles )
                          {   
                            $("#gallery").empty();
                            disponibles.forEach(function(item)
                            {
                                $(".sabor_gallery").append(' <li class="ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle" id="'+item.COD_PRODUCTO+'" style="height: 120px"><h5 class="ui-widget-header">'+item.DESCRIPCION+'</h5><span class="bandeja" value="" id="'+item.COD_PRODUCTO+'"><img src="'+item.archivo+'" style="width:80px ; height: 70px;">  </span> </span></li> ');
                            });

                            var uAg = navigator.userAgent.toLowerCase();
                            var isMobile = !!uAg.match(/android|iphone|ipad|ipod|blackberry|symbianos/i);

                            if (!isMobile)
                            {
                              @include('frigos::admin.frigostorages.scriptDragDrop', ['lang' => $locale])                               
                            }
                            else
                            {
                              @include('frigos::admin.frigostorages.scriptMovil', ['lang' => $locale])                               
                            }
                          }
                      });
                  });
            });
    </script>
@stop