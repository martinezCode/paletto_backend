  $( document ).ready(function() 
  {
    var $gallery = $( "#gallery" ),
          $trash = $( "#trash" );
    var recycle_icon = "<a href='#' title='s' class='ui-icon ui-icon-trash'>Recycle image</a>";

    $( "#gallery > li" ).on( "click", function( event ) 
    {
        var $gallery = $( "#gallery" ),
          $trash = $( "#trash" );
        $item = $( this );
        var codigo_config = $("select[name=CONFIGURACION_FRIGO]").val();
        var item =  $('input[name=COD_FRIGO]').val();

        $('#stock-save').click();
        $('input[name=stock_max]').val();
        $('input[name=config_contenedor_prod]').val(codigo_config);
        $('input[name=codigo_item_prod]').val(item);
        deleteImage($item);
    }); 

    $( "#trash_galery > li" ).on( "click", function( event ) 
                { 
                  var $item = $( this ),
                  $target = $( event.target );
                  // console.log($target);
                  
                  if ( $target.is( "a.ui-icon-trash" ) ) 
                  {
                    var codigo_frigo = $("input[name=COD_FRIGO]").val();
                    var codigo_configuracion = $("select[name=CONFIGURACION_FRIGO]").val();
                    var codigo_producto = $(this).find("span").attr("id");
              
                     $.ajax
                        ({
                            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                            url: '{!! route('admin.frigos.frigostorage.borrar_item') !!}',
                            type: 'POST',
                            data: {
                                    codigo_frigo: codigo_frigo, 
                                    codigo_configuracion: codigo_configuracion,
                                    codigo_producto: codigo_producto,
                                  }
                        });
                    recycleImage( $item );
                  }
                return false;
              }); 

    
  }); 

 function recycleImage( $item ) 
    {
      var $gallery = $( "#gallery" ),
          $trash = $( "#trash" );

      $item.fadeOut(function() 
          {
            $item
              .find( "a.ui-icon-trash" ).remove().end()
              .css( "width", "96px").find( "img" ).css( "height", "70px" ).end()
              .find("label").remove().end().find("b").remove().end()
              // .appendTo( $gallery )
              // .fadeIn();
          });

       $.ajax
          ({
              headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
              url: '{!! route('admin.frigos.frigostorage.sabores_disponibles') !!}',
              type: 'POST',
              data: {
                      codigo_frigo: codigo_frigo, 
                      codigo_configuracion: configuracion,
                    }, 
              dataType: 'json',    
              success: function( disponibles )
              {   
                $("#gallery").empty();
                disponibles.forEach(function(item)
                {
                    $(".sabor_gallery").append(' <li class="ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle" id="'+item.COD_PRODUCTO+'" style="height: auto"><h5 class="ui-widget-header">'+item.DESCRIPCION+'</h5><span class="bandeja" value="" id="'+item.COD_PRODUCTO+'"><img src="'+item.archivo+'" style="width:80px ; height: 70px;">  </span> </span></li> ');
                });

               $( "#gallery > li" ).on( "click", function( event ) 
                {
                    var $gallery = $( "#gallery" ),
                      $trash = $( "#trash" );
                    $item = $( this );
                    var codigo_config = $("select[name=CONFIGURACION_FRIGO]").val();
                    var item =  $('input[name=COD_FRIGO]').val();

                    $('#stock-save').click();
                    $('input[name=stock_max]').val();
                    $('input[name=config_contenedor_prod]').val(codigo_config);
                    $('input[name=codigo_item_prod]').val(item);
                    deleteImage($item);
                }); 
            }
        });
      
    }

    function deleteImage( $item ) 
    {
      var $gallery = $( "#gallery" ),
          $trash = $( "#trash" );
      $item.fadeOut(function() 
      {
        var $list = $( "ul", $trash ).length ?
          $( "ul", $trash ) :
          $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );

        var item  = $item.find("h5").html();
        var producto =  $item.attr("id");           

        $('input[name=descripcion_prod]').val(item);
        $('input[name=producto_id]').val(producto);
        $item.find( "a.ui-icon-trash" ).remove();
            
        $('#guardar').on( "click", function() 
        {
          // alert("presionado");
          var recycle_icon = "<a href='#' title='s' class='ui-icon ui-icon-trash'>Recycle image</a>";
          var stock = $('#stock_max').val();
          
          var stock_label = "<b>Sugerido: </b><label class='text-left' style='color:#ec4742;'> "+stock+"</label>";
          var imagy_footer ="<table><tr><td>"+stock_label+"</td><td>"  +recycle_icon+"</td></tr></table>";
          $item.appendTo( $list ).fadeIn(function() 
          {
            $item
            .append(imagy_footer);
          });


      var codigo_frigo =  $('input[name=COD_FRIGO]').val();
      var configuracion =  $('select[name=CONFIGURACION_FRIGO]').val();


//-----------------------------Ajax que carga los sabores ya contenidos---------------------------------------------//

          $.ajax
          ({
              headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
              url: '{!! route('admin.frigos.frigostorage.contenido_item') !!}',
              type: 'POST',
              data: {
                      codigo_frigo: codigo_frigo, 
                      codigo_configuracion: configuracion
                    }, 
              dataType: 'json',    
              success: function( contenidos )
              {   
                // var esto_quite_del_foreach = "<a title="Quitar Sabor" class="ui-icon ui-icon-trash"></a>";
                var recycle_icon = "<a title='Quitar Sabor' class='ui-icon ui-icon-trash'></a>"

                $(".trash_galery").empty();
                contenidos.forEach(function(element)
                {
                  var stock = element.STOCK_MAX;
                  var stock_label = "<b>Sugerido: </b><label class='text-left' style='color:#ec4742;'> "+stock+"</label>";
                  var imagy_footer ="<table><tr><td>"+stock_label+"</td><td> "+recycle_icon+"</td></tr></table>";

                    $("#trash_galery").append(' <li class="ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle" id="'+element.COD_PRODUCTO+'" style="height: auto"><h5 class="ui-widget-header">'+element.DESCRIPCION+'</h5><span class="bandeja" value="Bandeja " id="'+element.COD_PRODUCTO+'"><img src="'+element.archivo+'" style="width:80px ; height: 70px;"></span>'+imagy_footer+'</li>');
                });
                $( "#trash_galery > li" ).on( "click", function( event ) 
                { 
                  var $item = $( this ),
                  $target = $( event.target );
                  // console.log($target);
                  
                  if ( $target.is( "a.ui-icon-trash" ) ) 
                  {
                    var codigo_frigo = $("input[name=COD_FRIGO]").val();
                    var codigo_configuracion = $("select[name=CONFIGURACION_FRIGO]").val();
                    var codigo_producto = $(this).find("span").attr("id");
              
                     $.ajax
                        ({
                            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                            url: '{!! route('admin.frigos.frigostorage.borrar_item') !!}',
                            type: 'POST',
                            data: {
                                    codigo_frigo: codigo_frigo, 
                                    codigo_configuracion: codigo_configuracion,
                                    codigo_producto: codigo_producto,
                                  }
                        });
                    recycleImage( $item );
                  }
                return false;
              });


              }
          });
        });
      });
    }   

        