
{{-- <script type="text/javascript"> --}}
    $( document ).ready(function() 
    {
       // There's the gallery and the trash
        var $gallery = $( "#gallery" ),
          $trash = $( "#trash" );
     
        // Let the gallery items be draggable
        $( "li", $gallery ).draggable({
          cancel: "a.ui-icon", // clicking an icon won't initiate dragging
          revert: "invalid", // when not dropped, the item will revert back to its initial position
          containment: "document",
          helper: "clone",
          cursor: "move"
        });
     
        // Let the trash be droppable, accepting the gallery items
        $trash.droppable({
          accept: "#gallery > li",
          classes: {
            "ui-droppable-active": "ui-state-highlight"
          },
          drop: function( event, ui ) {
            
            var codigo_config = $("select[name=CONFIGURACION_FRIGO]").val();
            var item =  $('input[name=COD_FRIGO]').val();
            // alert(item);
           
            $('#stock-save').click();
            $('input[name=stock_max]').val();
            $('input[name=config_contenedor_prod]').val(codigo_config);
            $('input[name=codigo_item_prod]').val(item);
            
            deleteImage( ui.draggable );
          }
        });
     
        // Let the gallery be droppable as well, accepting items from the trash
          $gallery.droppable({
          accept: "#trash li",
          classes: 
          {
            "ui-droppable-active": "custom-state-active"
          },
            drop: function( event, ui ) {

              recycleImage( ui.draggable );
            }
        });
     
        // Image deletion function
        var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Quitar Sabor' class='ui-icon ui-icon-trash'>Recycle image</a>";

        function deleteImage( $item ) {
          $item.fadeOut(function() {
            var $list = $( "ul", $trash ).length ?
              $( "ul", $trash ) :
              $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );

            var item  = $item.find("h5").html();
            var producto =  $item.attr("id");           

            $('input[name=descripcion_prod]').val(item);
            $('input[name=producto_id]').val(producto);
            // $item.find( "a.ui-icon-trash" ).remove();

            $('#guardar').on( "click", function() 
            {
              var stock = $('#stock_max').val();
              
              var stock_label = "<b>Sugerido: </b><label class='text-left' style='color:#ec4742;'> "+stock+"</label>";
              var imagy_footer ="<table><tr><td>"+stock_label+"</td><td>"  +recycle_icon+"</td></tr></table>";
              // alert($item.find("label").html());
              $item.appendTo( $list ).fadeIn(function() 
              {
                if ($item.find("label").html() == undefined ) 
                {
                  $item
                  .append(imagy_footer)
                  .animate({ width: "102px", height:"150px" })
                  .find( "img" )
                  .animate({ height: "70px" });
                }
                else
                {
                  $item
                  // .append(imagy_footer)
                  .animate({ width: "102px" })
                  .find( "img" )
                  .animate({ height: "70px" });
                }
                
               });
            });
          });

        }
     
        // Image recycle function
        //var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";
        function recycleImage( $item ) 
        {
          $item.fadeOut(function() 
          {
        
            $item
              .find( "a.ui-icon-trash" ).remove().end()
              .css( "width", "96px")
              .css( "height", "120px")
              .find( "img" ).css( "height", "70px" ).end()
              .find("label").remove().end()
              .find("b").remove().end()
              .appendTo( $gallery )
              .fadeIn();
          });
          $item.fadeIn(function() 
          {
            $item.draggable({
              cancel: "a.ui-icon", // clicking an icon won't initiate dragging
              revert: "invalid", // when not dropped, the item will revert back to its initial position
              containment: "document",
              helper: "clone",
              cursor: "move"
            });
          });


        }
     
        // Resolve the icons behavior with event delegation
        $( "ul.gallery > li" ).on( "click", function( event ) {
          var $item = $( this ),
            $target = $( event.target );
     
          if ( $target.is( "a.ui-icon-trash" ) ) 
          {
            var codigo_frigo = $("input[name=COD_FRIGO]").val();
            var codigo_configuracion = $("select[name=CONFIGURACION_FRIGO]").val();
            var codigo_producto = $(this).find("span").attr("id");
      
             $.ajax
                ({
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    url: '{!! route('admin.frigos.frigostorage.borrar_item') !!}',
                    type: 'POST',
                    data: {
                            codigo_frigo: codigo_frigo, 
                            codigo_configuracion: codigo_configuracion,
                            codigo_producto: codigo_producto,
                          },    
                    success: function( contenidos )
                    { 

                    }
                });
            recycleImage( $item );
          }
          
          return false;
        });
  });
