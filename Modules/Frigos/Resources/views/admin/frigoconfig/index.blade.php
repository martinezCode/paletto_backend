@extends('layouts.master')

@section('content-header')
  
@stop

@section('content')
    <br>
      <h1>
        Configuracion de Frigos
    </h1>

    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">

                    <a href="{{ route('admin.frigos.frigoconfig.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> Crear Configuracion
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
     
                    {!! Form::open(array('route' => ['admin.frigos.frigoconfig.index_ajax'],'method' => 'post', 'id' => 'search-form')) !!}
                        <div class="row">
                            <div class="col-md-2 col-xs-4">
                                {!! Form::normalInput('nombre_frigo', 'Frigo', $errors, null, ['id' => 'nombre_frigo']) !!}
                            </div>

                            <div class="col-md-2 col-xs-4">
                                {!! Form::normalInput('codigo_frigo', 'Codigo Frigo', $errors, null, ['id' => 'codigo_frigo']) !!}
                            </div>

                            <div class="col-md-2 col-xs-4">
                                {!! Form::normalInput('descripcion', 'Configuracion', $errors, null, ['id' => 'descripcion']) !!}
                            </div >

                        </div>
                    {!! Form::close() !!}

                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="tablaFrigos" >
                            <thead>
                            <tr>
                                <th style="display: none;">id</th>
                                <th >Frigo</th>
                                <th>Codigo de Frigo</th>
                                <th>Configuracion</th>
                                <th>Ultima Modificacion</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                <tr>
                                    <td ></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                            </tbody>
                            <tfoot>
                            <tr>
                                <th style="display: none;">id</th>
                                <th>Frigo</th>
                                <th>Codigo de Frigo</th>
                                <th>Configuracion</th>
                                <th>Ultima Modificacion</th>
                                <th>Acciones</th>
                            </tr>

                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('clientes::clientes.title.create cliente') }}</dd>
    </dl>
@stop
<?php $locale = locale(); ?>
@section('scripts')
    
    <script type="text/javascript">


        $(document).ready(function()
        {
            $("input[name=nombre_frigo]").keyup(function()
            {
                $("#search-form").submit();
            });

            $("#codigo_frigo").on("keyup",function()
            {
                $("#search-form").submit();
            });

            $("#descripcion").on("keyup",function()
            {
                $("#search-form").submit();
            });

            $('#search-form').submit(function(e) 
            {
                table.draw();
                e.preventDefault();
            });

            var table = $('.data-table').DataTable(
            {
                dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                "<'row'<'col-xs-12't>>"+
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                "deferRender": true,
                'responsive': true,
                processing: false,
                serverSide: true,
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": false,
                "paginate": true,
                 "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ;?>'
                },
                ajax: 
                 {
                    url: '{!! route('admin.frigos.frigoconfig.index_ajax') !!}',
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    data: function (e) 
                    {
                        e.nombre_frigo = $('#nombre_frigo').val();
                        e.codigo_frigo = $('#codigo_frigo').val();
                        e.descripcion = $('#descripcion').val();
                        // e.ruc = $('#ruc').val();
                    }
                },
                columns: 
                [
                    { data: 'id', name: 'id' , visible: false },
                    { data: 'nombre_frigo', name: 'nombre_frigo' },
                    { data: 'codigo_frigo', name: 'codigo_frigo' },
                    { data: 'configuracion', name: 'configuracion' },
                    { data: 'usuario', name: 'usuario' },
                    { data: 'acciones', name: 'acciones', orderable: false, searchable: false} 
                ], 
            });
        });
    </script>
@stop
