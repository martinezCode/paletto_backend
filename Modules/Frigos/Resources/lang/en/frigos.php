<?php

return [
    'title' => [
        'frigos' => 'Frigo',
        'create frigo' => 'Create a frigo',
        'edit frigo' => 'Edit a frigo',
    ],
    'button' => [
        'create frigo' => 'Create a frigo',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
