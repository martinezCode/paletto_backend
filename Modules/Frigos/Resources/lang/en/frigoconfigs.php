<?php

return [
    'title' => [
        'frigoconfigs' => 'FrigoConfig',
        'create frigoconfig' => 'Create a frigoconfig',
        'edit frigoconfig' => 'Edit a frigoconfig',
    ],
    'button' => [
        'create frigoconfig' => 'Create a frigoconfig',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
