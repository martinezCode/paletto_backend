<?php

return [
    'title' => [
        'frigostorages' => 'FrigoStorage',
        'create frigostorage' => 'Create a frigostorage',
        'edit frigostorage' => 'Edit a frigostorage',
    ],
    'button' => [
        'create frigostorage' => 'Create a frigostorage',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
