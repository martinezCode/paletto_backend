<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrigosFrigosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('frigos__frigo', function(Blueprint $table) {
			$table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('DESCRIPCION');
            $table->string('COD_FRIGO');
            // $table->string('bandeja');
            // $table->string('caja');
            $table->string('USER');	
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('frigos__frigo');
	}
}
