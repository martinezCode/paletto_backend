<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrigosFrigostoragesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('frigos__Stock_Frigo_Config_Storage_Prod', function(Blueprint $table) {
			$table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('COD_FRIGO');
            $table->string('COD_CONFIG');
            $table->string('COD_GRAN_STORAGE');
            $table->string('COD_STORAGE');
            $table->string('COD_PRODUCTO');
            $table->string('DESCRIPCION');
            $table->string('STOCK_MIN');
            $table->string('STOCK_MAX');
            $table->string('idem');
            $table->string('user');	
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('frigos__Stock_Frigo_Config_Storage_Prod');
	}
}

