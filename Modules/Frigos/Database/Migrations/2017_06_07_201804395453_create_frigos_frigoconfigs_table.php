<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrigosFrigoconfigsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('frigos__frigoconfig', function(Blueprint $table) {
			$table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('COD_FRIGO');
            $table->string('COD_CONFIG');
            $table->string('DESCRIPCION');
            $table->string('idem');
            $table->string('user');	
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('frigos__frigoconfig');
	}
}
