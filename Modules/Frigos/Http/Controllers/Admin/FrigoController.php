<?php namespace Modules\Frigos\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Frigos\Entities\Frigo;
use Modules\User\Entities\Sentinel\User;
use Modules\Frigos\Repositories\FrigoRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Session,DB;

class FrigoController extends AdminBaseController
{
    /**
     * @var FrigoRepository
     */
    private $frigo;

    public function __construct(FrigoRepository $frigo)
    {
        parent::__construct();

        $this->frigo = $frigo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // $frigos = $this->frigo->all();
        $frigos = Frigo::orderBy("FECHA")->get();
        // dd($frigos->get());

        return view('frigos::admin.frigos.index', compact('frigos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {   
        $user = Session::get('user_mail');
        $name = User::where('email',$user)->first()->first_name;
        $last_name = User::where('email',$user)->first()->last_name;
        $usuario = $name.$last_name;
        // dd($usuario );
        return view('frigos::admin.frigos.create', compact('usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request->all());    
        $request['codigo_frigo'] = str_replace(' ', '', $request['descripcion']); 
        Frigo::insert([
                "DESCRIPCION"=>$request['descripcion'],
                "COD_FRIGO"=>$request['codigo_frigo'],
                "USER"=>$request['user']
            ]);

        //$frigo = $this->frigo->create($request->all());
        // dd($this->frigo);
        flash()->success(trans('Frigo Creado Correctamente'));

       return redirect()->route('admin.frigos.frigo.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Frigo $frigo
     * @return Response
     */
    public function edit(Frigo $frigo)
    {
        $user = Session::get('user_mail');
        // dd($user);
        $name = User::where('email',$user)->first()->first_name;
        $last_name = User::where('email',$user)->first()->last_name;
        $usuario = $name.$last_name;

        return view('frigos::admin.frigos.edit', compact('frigo', 'usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Frigo $frigo
     * @param  Request $request
     * @return Response
     */
    public function update(Frigo $frigo, Request $request)
    {
        $request['COD_FRIGO'] = str_replace(' ', '', $request['DESCRIPCION']); 

        $this->frigo->update($frigo, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('frigos::frigos.title.frigos')]));

        return redirect()->route('admin.frigos.frigo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Frigo $frigo
     * @return Response
     */
    public function destroy(Frigo $frigo)
    {
        $this->frigo->destroy($frigo);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('frigos::frigos.title.frigos')]));

        return redirect()->route('admin.frigos.frigo.index');
    }
}
