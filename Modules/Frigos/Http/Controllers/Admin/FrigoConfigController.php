<?php namespace Modules\Frigos\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Frigos\Entities\Frigo;
use Modules\User\Entities\Sentinel\User;
use Modules\Frigos\Entities\FrigoConfig;
use Modules\Frigos\Repositories\FrigoRepository;
use Modules\Frigos\Repositories\FrigoConfigRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Yajra\Datatables\Facades\Datatables;        
use DB,Session;

class FrigoConfigController extends AdminBaseController
{
    /**
     * @var FrigoConfigRepository
     */
    private $frigoconfigs;

    public function __construct(FrigoConfigRepository $frigoconfigs)
    {
        parent::__construct();

        $this->frigoconfigs = $frigoconfigs;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // dd("vsdfvsd");
        return view('frigos::admin.frigoconfig.index');
    }

    public function index_ajax(Request $request)
    {
        $query = DB::table('Frigo_Config')->join('Frigos','Frigos.COD_FRIGO','=','Frigo_Config.cod_frigo')
                   ->select(
                    [
                        'Frigo_Config.id as id',
                        'Frigos.DESCRIPCION as nombre_frigo',
                        'Frigos.COD_FRIGO as codigo_frigo',
                        'Frigo_Config.DESCRIPCION as configuracion',
                        'Frigo_Config.user as usuario',
                    ]);
        // dd($request->all());
        $clientes = Datatables::of( $query )
            ->addColumn('acciones', function ($tabla) 
            {
                $asEdit = "admin.frigos.frigoconfig.edit";

                $asDestroy = "admin.frigos.frigoconfig.destroy";

                $editRoute = route( $asEdit, [$tabla->id]);

                $deleteRoute = route( $asDestroy, [$tabla->id]);

                $buttons="<div class='btn-group'>
                            <a href='". route( $asEdit, [$tabla->id])." ' class='btn btn-default btn-flat'>
                                <i class='fa fa-pencil'></i>
                            </a>
                            
                            <button class='btn btn-danger btn-flat' data-toggle='modal' data-target='#modal-delete-confirmation' data-action-target='". $deleteRoute ."'>
                                <i class='fa fa-trash'></i>
                            </button>
                            
                        </div>";

                return $buttons;
            })
            ->filter(function ($query) use ($request)
            {
                if ($request->has('nombre_frigo')  && trim($request->has('nombre_frigo') !== '') )
                {
                    $query->where('Frigo.descripcion', 'like', "%{$request->get('nombre_frigo')}%");
                }

                if ($request->has('codigo_frigo')  && trim($request->has('codigo_frigo') !== '') )
                {
                    $query->where('Frigo.codigo_frigo', 'like', "%{$request->get('codigo_frigo')}%");
                }

                if ($request->has('configuracion')  && trim($request->has('configuracion') !== '') )
                {
                    $query->where('Frigo_Config.descripcion', 'like', "%{$request->get('configuracion')}%");
                }
            })
            ->make(true);

        return $clientes;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Session::get('user_mail');

        $name = User::where('email',$user)->first()->first_name;
        
        $last_name = User::where('email',$user)->first()->last_name;
        
        $usuario = $name.$last_name;

        $frigos = Frigo::lists('DESCRIPCION','cod_frigo')->all();
        
        return view('frigos::admin.frigoconfig.create', compact('usuario', 'frigos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {   
        $last_config = FrigoConfig::orderBy('COD_CONFIG','desc')->where('COD_FRIGO',$request["Frigo"])->first()->COD_CONFIG;
        // dd($last_config);
        if (!$last_config) 
        { 
            $request['idem'] = $request['Frigo']."1";

            DB::table('Frigo_Config')->insert
            ([
                'cod_frigo' => $request['Frigo'],
                'cod_config' => 1,
                'descripcion' => $request['descripcion'],
                'idem' => $request['idem'],
                'user' => $request['user'],
            ]);                 
        }
        else
        {
            $last_config = $last_config +1;
            // dd($last_config);

            $request['idem'] = $request['Frigo'].$last_config;
            // dd($request['idem']);
            DB::table('Frigo_Config')->insert
            ([
                'COD_FRIGO' => $request['Frigo'],
                'COD_CONFIG' => $last_config,
                'DESCRIPCION' => $request['descripcion'],
                'idem' => $request['idem'],
                'user' => $request['user'],
            ]);
        }
        
        flash()->success(trans('Configuracion Agregada Corectamente'));

        return redirect()->route('admin.frigos.frigoconfig.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Frigo $frigo
     * @return Response
     */
    public function edit(FrigoConfig $frigoconfigs , Request $request)
    {
        $user = Session::get('user_mail');
        $name = User::where('email',$user)->first()->first_name;
        $last_name = User::where('email',$user)->first()->last_name;
        
        $usuario = $name.$last_name;

        $frigo = Frigo::lists('DESCRIPCION','COD_FRIGO')->all();

        return view('frigos::admin.frigoconfig.edit', compact('frigoconfigs', 'usuario' , 'frigo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Frigo $frigo
     * @param  Request $request
     * @return Response
     */
    public function update(Frigo $frigo, Request $request)
    {
        $this->frigo->update($frigo, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('frigos::frigos.title.frigos')]));

        return redirect()->route('admin.frigos.frigo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Frigo $frigo
     * @return Response
     */
    public function destroy(FrigoConfig $frigoconfigs)
    {

        $this->frigoconfigs->destroy($frigoconfigs);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('frigos::frigos.title.frigos')]));

        return redirect()->route('admin.frigos.frigo.index');
    }
}
