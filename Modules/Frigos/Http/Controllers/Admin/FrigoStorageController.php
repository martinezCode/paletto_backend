<?php namespace Modules\Frigos\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Frigos\Entities\FrigoStorage;
use Modules\User\Entities\Sentinel\User;
use Modules\Productos\Entities\Producto;
use Modules\Frigos\Entities\FrigoConfig;
use Modules\Frigos\Entities\Frigo;
use Modules\Frigos\Repositories\FrigoStorageRepository;
use Modules\Frigos\Repositories\FrigoRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Yajra\Datatables\Facades\Datatables; 
use Session,DB;

class FrigoStorageController extends AdminBaseController
{
    /**
     * @var FrigoStorageRepository
     */
    private $frigostorage;

    /**
     * @var FrigoRepository
     */
    private $frigo;

    public function __construct(FrigoStorageRepository $frigostorage , FrigoRepository $frigo)
    {
        parent::__construct();

        $this->frigostorage = $frigostorage;

        $this->frigo = $frigo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$frigostorages = $this->frigostorage->all();

        return view('frigos::admin.frigostorages.index', compact(''));
    }

    public function index_ajax(Request $request)
    {
        $query = Frigo::join("Frigo_Config","Frigo_Config.COD_FRIGO","=","Frigos.COD_FRIGO")
                    ->groupBy("Frigos.id")
                    ->select(
                    [
                        'Frigos.id as id',
                        'Frigos.DESCRIPCION as nombre_frigo',
                        'Frigos.COD_FRIGO as codigo_frigo',
                        // 'Frigo_Config.descripcion as configuracion',
                    ]);
        // dd($request->all());
        $storages = Datatables::of( $query )
            ->addColumn('acciones', function ($tabla) 
            {
                $asEdit = "admin.frigos.frigostorage.edit";

                $asDestroy = "admin.frigos.frigostorage.destroy";
                
                $contenido = "admin.frigos.frigostorage.storage";

                $editRoute = route( $asEdit, [$tabla->id]);

                $deleteRoute = route( $asDestroy, [$tabla->id]);

                $buttons="<div class='btn-group'>

                            <a href='". route( $contenido, [$tabla->id])." ' class='btn btn-primary btn-flat'>
                                <b>Ver Contenido</b>
                            </a>
                            
                        </div>";

                return $buttons;
            })
            ->filter(function ($query) use ($request)
            {
                if ($request->has('nombre_frigo')  && trim($request->has('nombre_frigo') !== '') )
                {
                    $query->where('Frigo.descripcion', 'like', "%{$request->get('nombre_frigo')}%");
                }
            })
            ->make(true);

        return $storages;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $configuraciones = FrigoConfig::lists("DESCRIPCION","COD_CONFIG")->all();
        return view('frigos::admin.frigostorages.storage' , compact("configuraciones"));
    }

     public function contenido_item(Request $request)
    {
        $items_id = array();
        $contenido =  array();
        $cod_frigo = $request["codigo_frigo"];
        $cod_config = $request["codigo_configuracion"];

        $contenido_item = Producto::join("Stock_Frigo_Config_Storage_Prod","Stock_Frigo_Config_Storage_Prod.COD_PRODUCTO","=","Productos.COD_PRODUCTO")
                        ->where("COD_FRIGO",$cod_frigo)
                        ->where("COD_CONFIG",$cod_config)
                        ->select
                        ([
                            "Productos.id as id",
                            "Productos.DESCRIPCION as DESCRIPCION",
                            "Stock_Frigo_Config_Storage_Prod.COD_FRIGO as COD_FRIGO",
                            "Stock_Frigo_Config_Storage_Prod.COD_CONFIG as COD_CONFIG",
                            "Productos.COD_PRODUCTO as COD_PRODUCTO",
                            "Stock_Frigo_Config_Storage_Prod.STOCK_MAX as STOCK_MAX",
                            ])
                        ->get();

        // foreach ($contenido_item as $key => $item) 
        // {
        //     $contenido[]= Producto::where("COD_PRODUCTO",$item->COD_PRODUCTO)->first();
        // }
        // dd($contenido_item);
        return response()->json($contenido_item);
    }

    public function sabores_disponibles(Request $request)
    {
        // dd($request->all()   );
        $sabores_id = array();
        $contenido =  array();
        $cod_frigo = $request["codigo_frigo"];
        $cod_config = $request["codigo_configuracion"];

        $contenido_item = FrigoStorage::where("COD_FRIGO",$cod_frigo)
                        ->where("COD_CONFIG",$cod_config)
                        ->get();
        // dd($contenido_item);
        foreach ($contenido_item as $key => $item) 
        {
            $aux_contenido = Producto::where("COD_PRODUCTO",$item->COD_PRODUCTO)->first();
            
            if ($aux_contenido != null) 
            {
                $contenido[]= $aux_contenido->COD_PRODUCTO;
            }
        }
        // dd($contenido);

<<<<<<< HEAD
        $sabores = Producto::whereNotIn('COD_PRODUCTO', $contenido)
        ->where("ACTIVO",true)
        ->get();
                  
=======
        $sabores = Producto::whereNotIn('COD_PRODUCTO', $contenido)->where("ACTIVO",true)->get();
        
           
>>>>>>> 723e2cc45c50244d3d09a9e4a20c8153b15e792d
        return response()->json($sabores);
    }

    public function borrar_item(Request $request)
    {
        $item = FrigoStorage::where("COD_FRIGO",$request["codigo_frigo"])
                    ->where("COD_CONFIG",$request["codigo_configuracion"])
                    ->where("COD_PRODUCTO",$request["codigo_producto"])
                    ->first()->id;
        if($item)
        {
            FrigoStorage::where('id', $item)->delete();
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->frigostorage->create($request->all());

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('frigos::frigostorages.title.frigostorages')]));

        return redirect()->route('admin.frigos.frigostorage.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  FrigoStorage $frigostorage
     * @return Response
     */
    public function storage(Frigo $frigo)
    {
        // dd($frigo);
        $frigo_configs = FrigoConfig::where("COD_FRIGO",$frigo->COD_FRIGO)->first();
        $array_configs = FrigoConfig::where("COD_FRIGO",$frigo->COD_FRIGO)->lists("DESCRIPCION","COD_CONFIG")->all();
        // dd($array_configs);

        return view('frigos::admin.frigostorages.storage', compact('frigostorage','frigo' , 'array_configs'));
    }

    public function store_item_content(Request $request)
    {   
        // dd($request->all());
        //OBTENER EL USUARIO
        $user = Session::get('user_mail');
        $name = User::where('email',$user)->first()->first_name;
        $last_name = User::where('email',$user)->first()->last_name;
        $usuario = $name.$last_name;     
        //Aqui se obtiene los datos para volver a recargar la pagina
        $producto_id = $request["producto_id"]; 
        $producto = $request["descripcion_prod"];
        $config_frigo = $request["config_contenedor_prod"];
        $codigo_frigo = $request["codigo_item_prod"];
        $frigo = Frigo::where("COD_FRIGO",$codigo_frigo)->first();
        $array_configs = FrigoConfig::where("COD_FRIGO",$codigo_frigo)->lists("DESCRIPCION","COD_CONFIG")->all();
        $frigos_config = DB::table("Frigo_Config")
                       ->where('COD_FRIGO',$codigo_frigo)
                       ->lists("DESCRIPCION","cod_config");
        $productos = Producto::get();

        $query = FrigoStorage::where("idem", $codigo_frigo.$config_frigo."B1"."B1".$producto_id)
                    ->first();
       
        // Aca se guardan los datos del item agregado recientemente
        // DB::beginTransaction();

            if(!$query)
            {
                try
                {
                    DB::table("Stock_Frigo_Config_Storage_Prod")
                    ->insert([
                            "COD_FRIGO"=>$codigo_frigo,
                            "COD_CONFIG"=>$config_frigo,
                            "COD_GRAN_STORAGE"=>"B1",
                            "COD_STORAGE"=>"B1",
                            "COD_PRODUCTO"=>$producto_id,
                            "DESCRIPCION"=>$producto,
                            "STOCK_MIN"=>0,
                            "STOCK_MAX"=>$request["stock_max"],
                            "idem"=>$codigo_frigo.$config_frigo."B1"."B1".$producto_id,
                            "user"=>$usuario
                        ]);

                }catch (ValidationException $e)
                {
                    // DB::rollBack();
                    // //dd($e);
                    // return redirect()->back()->withErrors($e);
                }
            }
            else
            {
                try
                {
                    DB::table("Stock_Frigo_Config_Storage_Prod")
                    ->where("id",$query->id)
                    ->update([
                            "COD_FRIGO"=>$codigo_frigo,
                            "COD_CONFIG"=>$config_frigo,
                            "COD_GRAN_STORAGE"=>"B1",
                            "COD_STORAGE"=>"B1",
                            "COD_PRODUCTO"=>$producto_id,
                            "DESCRIPCION"=>$producto,
                            "STOCK_MIN"=>0,
                            "STOCK_MAX"=>$request["stock_max"],
                            "idem"=>$codigo_frigo.$config_frigo."B1"."B1".$producto_id,
                            "user"=>$usuario
                        ]);

                }catch (ValidationException $e)
                {
                    // DB::rollBack();
                    // //dd($e);
                    // return redirect()->back()->withErrors($e);
                }
            }
        dd("Terminado");
            // DB::commit();

            // flash()->success("Storage Guardado Correctamente");
            
            // return view('frigos::admin.frigostorages.storage' , compact('frigo', 'frigos_config',"nombre_item","productos","codigo_frigo", "tipo_item" , "array_configs"));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  FrigoStorage $frigostorage
     * @param  Request $request
     * @return Response
     */
    public function update(FrigoStorage $frigostorage, Request $request)
    {
        $this->frigostorage->update($frigostorage, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('frigos::frigostorages.title.frigostorages')]));

        return redirect()->route('admin.frigos.frigostorage.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  FrigoStorage $frigostorage
     * @return Response
     */
    public function destroy(FrigoStorage $frigostorage)
    {
        $this->frigostorage->destroy($frigostorage);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('frigos::frigostorages.title.frigostorages')]));

        return redirect()->route('admin.frigos.frigostorage.index');
    }
}
