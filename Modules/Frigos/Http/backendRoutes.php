<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/frigos'], function (Router $router) {
    $router->bind('frigo', function ($id) {
        return app('Modules\Frigos\Repositories\FrigoRepository')->find($id);
    });
    $router->get('frigos', [
        'as' => 'admin.frigos.frigo.index',
        'uses' => 'FrigoController@index',
        'middleware' => 'can:frigos.frigos.index'
    ]);
    $router->get('frigos/create', [
        'as' => 'admin.frigos.frigo.create',
        'uses' => 'FrigoController@create',
        'middleware' => 'can:frigos.frigos.create'
    ]);
    $router->post('frigos', [
        'as' => 'admin.frigos.frigo.store',
        'uses' => 'FrigoController@store',
        'middleware' => 'can:frigos.frigos.store'
    ]);
    $router->get('frigos/{frigo}/edit', [
        'as' => 'admin.frigos.frigo.edit',
        'uses' => 'FrigoController@edit',
        'middleware' => 'can:frigos.frigos.edit'
    ]);
    $router->put('frigos/{frigo}', [
        'as' => 'admin.frigos.frigo.update',
        'uses' => 'FrigoController@update',
        'middleware' => 'can:frigos.frigos.update'
    ]);
    $router->delete('frigos/{frigo}', [
        'as' => 'admin.frigos.frigo.destroy',
        'uses' => 'FrigoController@destroy',
        'middleware' => 'can:frigos.frigos.destroy'
    ]);
   

    $router->bind('frigoconfigs', function ($id) {
        return app('Modules\Frigos\Repositories\FrigoConfigRepository')->find($id);
    });
    $router->get('frigoconfigs', [
        'as' => 'admin.frigos.frigoconfig.index',
        'uses' => 'FrigoConfigController@index',
        'middleware' => 'can:frigos.frigoconfigs.index'
    ]);

    $router->post('frigoconfigs/index_ajax', [
        'as' => 'admin.frigos.frigoconfig.index_ajax',
        'uses' => 'FrigoConfigController@index_ajax',
        'middleware' => 'can:frigos.frigoconfigs.index_ajax'
    ]);

    $router->get('frigoconfigs/create', [
        'as' => 'admin.frigos.frigoconfig.create',
        'uses' => 'FrigoConfigController@create',
        'middleware' => 'can:frigos.frigoconfigs.create'
    ]);
    $router->post('frigoconfigs', [
        'as' => 'admin.frigos.frigoconfig.store',
        'uses' => 'FrigoConfigController@store',
        'middleware' => 'can:frigos.frigoconfigs.store'
    ]);
    $router->get('frigoconfigs/{frigoconfigs}/edit', [
        'as' => 'admin.frigos.frigoconfig.edit',
        'uses' => 'FrigoConfigController@edit',
        'middleware' => 'can:frigos.frigoconfigs.edit'
    ]);
    $router->put('frigoconfigs/{frigoconfigs}', [
        'as' => 'admin.frigos.frigoconfig.update',
        'uses' => 'FrigoConfigController@update',
        'middleware' => 'can:frigos.frigoconfigs.update'
    ]);
    $router->delete('frigoconfigs/{frigoconfigs}', [
        'as' => 'admin.frigos.frigoconfig.destroy',
        'uses' => 'FrigoConfigController@destroy',
        'middleware' => 'can:frigos.frigoconfigs.destroy'
    ]);


    $router->bind('frigostorage', function ($id) {
        return app('Modules\Frigos\Repositories\FrigoStorageRepository')->find($id);
    });
    $router->get('frigostorages', [
        'as' => 'admin.frigos.frigostorage.index',
        'uses' => 'FrigoStorageController@index',
        'middleware' => 'can:frigos.frigostorages.index'
    ]);

    $router->post('frigostorages/index_ajax', [
        'as' => 'admin.frigos.frigostorage.index_ajax',
        'uses' => 'FrigoStorageController@index_ajax',
        'middleware' => 'can:frigos.frigostorages.index_ajax'
    ]);

    $router->get('frigostorages/create', [
        'as' => 'admin.frigos.frigostorage.create',
        'uses' => 'FrigoStorageController@create',
        'middleware' => 'can:frigos.frigostorages.create'
    ]);

    $router->get('frigostorages/{frigo}/storage', [
        'as' => 'admin.frigos.frigostorage.storage',
        'uses' => 'FrigoStorageController@storage',
        'middleware' => 'can:frigos.frigostorages.storage'
    ]);

    $router->post('frigostorages', [
        'as' => 'admin.frigos.frigostorage.store',
        'uses' => 'FrigoStorageController@store',
        'middleware' => 'can:frigos.frigostorages.store'
    ]);

    $router->get('frigostorages/{frigostorage}/edit', [
        'as' => 'admin.frigos.frigostorage.edit',
        'uses' => 'FrigoStorageController@edit',
        'middleware' => 'can:frigos.frigostorages.edit'
    ]);

    $router->put('frigostorages/{frigostorage}', [
        'as' => 'admin.frigos.frigostorage.update',
        'uses' => 'FrigoStorageController@update',
        'middleware' => 'can:frigos.frigostorages.update'
    ]);

    $router->delete('frigostorages/{frigostorage}', [
        'as' => 'admin.frigos.frigostorage.destroy',
        'uses' => 'FrigoStorageController@destroy',
        'middleware' => 'can:frigos.frigostorages.destroy'
    ]);

    $router->post('frigostorages/contenido_item', [
        'as' => 'admin.frigos.frigostorage.contenido_item',
        'uses' => 'FrigoStorageController@contenido_item',
        'middleware' => 'can:frigos.frigostorages.contenido_item'
    ]);

    $router->post('frigostorages/sabores_disponibles', [
        'as' => 'admin.frigos.frigostorage.sabores_disponibles',
        'uses' => 'FrigoStorageController@sabores_disponibles',
        'middleware' => 'can:frigos.frigostorages.sabores_disponibles'
    ]);

    $router->post('frigostorages/store_item_content', [
        'as' => 'admin.frigos.frigostorage.store_item_content',
        'uses' => 'FrigoStorageController@store_item_content',
        'middleware' => 'can:frigos.frigostorages.store_item_content'
    ]);

    $router->post('frigostorages/borrar_item', [
        'as' => 'admin.frigos.frigostorage.borrar_item',
        'uses' => 'FrigoStorageController@borrar_item',
        'middleware' => 'can:frigos.frigostorages.borrar_item'
    ]);
// append
});
