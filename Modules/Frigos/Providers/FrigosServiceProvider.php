<?php namespace Modules\Frigos\Providers;

use Illuminate\Support\ServiceProvider;

class FrigosServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Frigos\Repositories\FrigoRepository',
            function () {
                $repository = new \Modules\Frigos\Repositories\Eloquent\EloquentFrigoRepository(new \Modules\Frigos\Entities\Frigo());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Frigos\Repositories\Cache\CacheFrigoDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Frigos\Repositories\FrigoConfigRepository',
            function () {
                $repository = new \Modules\Frigos\Repositories\Eloquent\EloquentFrigoConfigRepository(new \Modules\Frigos\Entities\FrigoConfig());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Frigos\Repositories\Cache\CacheFrigoConfigDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Frigos\Repositories\FrigoStorageRepository',
            function () {
                $repository = new \Modules\Frigos\Repositories\Eloquent\EloquentFrigoStorageRepository(new \Modules\Frigos\Entities\FrigoStorage());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Frigos\Repositories\Cache\CacheFrigoStorageDecorator($repository);
            }
        );
// add bindings



    }
}
