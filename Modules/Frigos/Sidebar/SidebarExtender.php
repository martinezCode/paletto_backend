<?php namespace Modules\Frigos\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('Frigos'), function (Item $item) {
                $item->icon('fa fa-laptop');
                $item->weight(0);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('Administrar Frigos'), function (Item $item) {
                    $item->icon('');
                    $item->weight(0);
                    $item->append('admin.frigos.frigo.create');
                    $item->route('admin.frigos.frigo.index');
                    $item->authorize(
                        $this->auth->hasAccess('frigos.frigos.index')
                    );
                });
                $item->item(trans('Configurar Frigos'), function (Item $item) {
                    $item->icon('');
                    $item->weight(1);
                    $item->append('admin.frigos.frigoconfig.create');
                    $item->route('admin.frigos.frigoconfig.index');
                    $item->authorize(
                        $this->auth->hasAccess('frigos.frigoconfigs.index')
                    );
                });
                $item->item(trans('Almacenamiento Frigos'), function (Item $item) {
                    $item->icon('');
                    $item->weight(2);
                    // $item->append('admin.frigos.frigostorage.create');
                    $item->route('admin.frigos.frigostorage.index');
                    $item->authorize(
                        $this->auth->hasAccess('frigos.frigostorages.index')
                    );
                });
// append



            });
        });

        return $menu;
    }
}
